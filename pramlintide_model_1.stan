// Implementation of pramlintide submodel 

//      Up -> Q1 -> Q2 -> e
//                          
// Up all goes into Q1
// Q1 -> Q2 governed by Kp2p1
// Q2 -> e governed by Kep2

functions {

    vector pramlintide_pk_ode (real t, vector X, real [] ode_parameters) {

        int N_VARIABLES = 3;

        vector [N_VARIABLES] dXdt;

        real Kp2p1 = ode_parameters[1];
        real Kep2 = ode_parameters[2];

        // INDEX_* variables in transformed data apparently not available here. Oh well.

        real Q1 = X[1];
        real Q2 = X[2];
        real Qe = X[3];

        real dQ1_dt = -Kp2p1*Q1;
        real dQ2_dt = Kp2p1*Q1 - Kep2*Q2;
        real dQe_dt = Kep2*Q2;

        dXdt [1] = dQ1_dt;
        dXdt [2] = dQ2_dt;
        dXdt [3] = dQe_dt;

        return dXdt;
    }

    real [,] compute_states (real t0, real [] t, real [] init, real [] ode_parameters) {

        int N_VARIABLES = 3;
        int n = size (t);

        int n1 = t[1] == 0.0 ? n - 1 : n;
        real t1 [n1] = t [(t[1] == 0.0 ? 2 : 1):n];

        vector [N_VARIABLES] x0 = to_vector (init);

        real y [N_VARIABLES, n];

        vector [N_VARIABLES] x [n1] = ode_rk45 (pramlintide_pk_ode, x0, t0, t1, ode_parameters);

        if (t[1] == 0.0) {
            y [1:N_VARIABLES, 1] = init;
            for (i in 2:n) {
                y [1:N_VARIABLES, i] = to_array_1d (x[i - 1]);
            }
        }
        else {
            for (i in 1:n) {
                y [1:N_VARIABLES, i] = to_array_1d (x[i]);
            }
        }

        return y;
    }
}

data {

    int <lower = 0> N_SUBJECTS;
    real <lower = 0> Wgt [N_SUBJECTS];

    int <lower = 0> N_TIMESTEPS;
    real <lower = 0> t_timesteps [N_TIMESTEPS];

    int <lower = 0> N_UNCENSORED;
    int <lower = 0> timestep_uncensored [N_UNCENSORED];
    real <lower = 0> PCORRES_uncensored [N_UNCENSORED];
    int <lower = 0> index_first_PCORRES_uncensored [N_SUBJECTS];
    int <lower = 0> index_last_PCORRES_uncensored [N_SUBJECTS];

    int <lower = 0> N_CENSORED;
    int <lower = 0> timestep_censored [N_CENSORED];
    real <lower = 0> PCORRES_censored_below [N_CENSORED];
    int <lower = 0> index_first_PCORRES_censored_below [N_SUBJECTS];
    int <lower = 0> index_last_PCORRES_censored_below [N_SUBJECTS];

    real <lower = 0> Up;
}

transformed data {

    int N_VARIABLES = 3;

    int INDEX_Q1 = 1;
    int INDEX_Q2 = 2;
    int INDEX_QE = 3;
}

parameters {

    real <lower = 0.0112/3, upper = 9*0.0112> Kp2p1;
    real <lower = 0.0189/3, upper = 3*0.0189> Kep2;
    real <lower = 0.5645/3, upper = 3*0.5645> VI;
}

transformed parameters {

    real state_estimates [N_SUBJECTS, N_VARIABLES, N_TIMESTEPS];
    real state_estimates1 [N_VARIABLES, N_TIMESTEPS];

    real ode_parameters [N_SUBJECTS, 2];

    real Up_mU_kg;

    real initial_conditions [N_SUBJECTS, N_VARIABLES];

    for (i in 1:N_SUBJECTS) {

        ode_parameters [i, 1:2] = { Kp2p1, Kep2 };

        // Convert U to mU/kg.
        Up_mU_kg = Up * 1000 / Wgt[i];

        initial_conditions [i, 1:N_VARIABLES] = { Up_mU_kg, 0.0, 0.0, 0.0 };

        // print ("transformed parameters: just before call to compute_states w/ i = ", i);
        state_estimates1 = compute_states (0.0, t_timesteps, initial_conditions[i, 1:N_VARIABLES], ode_parameters[i, 1:2]);

        // for (j in 1:N_TIMESTEPS) {
        //     print ("state_estimates1[1:", N_VARIABLES, ", ", j, "] = ");
        //     print (state_estimates1[1:N_VARIABLES, j]);
        // }

        state_estimates [i, 1:N_VARIABLES, 1:N_TIMESTEPS] = state_estimates1;
    }
}

model {

    // PROBABLY S.D. SHOULD BE SYMBOLIC

    for (i in 1:N_SUBJECTS)
        for (j in index_first_PCORRES_uncensored[i]:index_last_PCORRES_uncensored[i])
            target += normal_lpdf (PCORRES_uncensored[j] | 34.7 * (state_estimates [i, INDEX_QI3, timestep_uncensored[j]] / VI), 200.0);

    for (i in 1:N_SUBJECTS)
        for (j in index_first_PCORRES_censored_below[i]:index_last_PCORRES_censored_below[i])
            target += normal_lcdf (PCORRES_censored_below[j] | 34.7 * (state_estimates [i, INDEX_QI3, timestep_censored[j]] / VI), 200.0);
}
